<?php
// Konfiguration Benutzerfelder
$config["cform"][]=array("type"=>"select","title"=>"Anrede","name"=>"anrede","placeholder"=>"Bevorzugte Anrede","required"=>true,"options"=>array("Herr","Frau"));
$config["cform"][]=array("type"=>"text","title"=>"Vorname Teilnehmer","name"=>"vornameteil","placeholder"=>"Bitte gebe Deinen Vornamen ein","required"=>true,"value"=>"Max");
$config["cform"][]=array("type"=>"text","title"=>"Nachname Teilnehmer","name"=>"nachnameteil","placeholder"=>"Bitte gebe Deinen Nachnamen ein","required"=>true,"value"=>"Mustermann");
$config["cform"][]=array("type"=>"text","title"=>"Geburtsdatum","name"=>"birthdate","placeholder"=>"Bitte gebe Dein Geburtsdatum ein","required"=>true,"id"=>"datepicker","value"=>"03.10.1998");
$config["cform"][]=array("type"=>"text","title"=>"Straße, Hausnummer","name"=>"adresse","placeholder"=>"Bitte gebe Deine Adresse ein","required"=>true,"value"=>"Musterstraße 3");
$config["cform"][]=array("type"=>"text","title"=>"PLZ, Stadt","name"=>"Stadt","placeholder"=>"Bitte gebe PLZ und Stadt","required"=>true,"value"=>"39619 Arendsee");
$config["cform"][]=array("type"=>"text","title"=>"Bundesland/Kanton","name"=>"bundesland","placeholder"=>"Bitte gebe Dein Bundesland/Kanton ein","required"=>false,"value"=>"Sachsen-Anhalt");
$config["cform"][]=array("type"=>"text","title"=>"Land","name"=>"land","placeholder"=>"Bitte gebe Dein Land ein","required"=>true,"value"=>"Qualityland");
$config["cform"][]=array("type"=>"text","title"=>"Telefonnr.","name"=>"telefon1","placeholder"=>"Bitte gebe Deine Telefonnummer ein","required"=>true,"value"=>"+491701234567");
$config["cform"][]=array("type"=>"text","title"=>"weitere Telefonnr. (z.B. Eltern)","name"=>"telefon2","placeholder"=>"Bitte gebe Deine Telefonnummer ein","required"=>false,"value"=>"+491701234567");
$config["cform"][]=array("type"=>"text","title"=>"Vorname Erziehungberechtigter (Bei Teilnehmern unter 18)","name"=>"vornameeltern","placeholder"=>"Bitte gebe Deinen Nachnamen ein","required"=>false,"value"=>"Max Mutter");
$config["cform"][]=array("type"=>"text","title"=>"Nachname Erziehungberechtigter (Bei Teilnehmern unter 18)","name"=>"nachnameeltern","placeholder"=>"Bitte gebe Deinen Nachnamen ein","required"=>false,"value"=>"Mustermann");
$config["cform"][]=array("type"=>"email","title"=>"Email Adresse","name"=>"email","placeholder"=>"Bitte gebe Deine Email ein","required"=>true,"value"=>"giovanni.zeitz@filmstadt-arendsee.de");
$config["cform"][]=array("type"=>"email","title"=>"Email Adresse bestätigen","name"=>"email_confirm","placeholder"=>"Bitte bestätige Deine Email","required"=>true,"value"=>"giovanni.zeitz@filmstadt-arendsee.de");
$config["cform"][]=array("type"=>"email2","title"=>"Weitere Email Adresse (z.B. Erziehungsberechtigte)","name"=>"email2","placeholder"=>"Bitte gebe Deine Email ein","required"=>false,"value"=>"giovanni.zeitz@jugendfilmcamp.de");
$config["cform"][]=array("type"=>"checkbox","title"=>"Ich war bereits im Camp (Ermäßigung)","name"=>"known");
// $config["cform"][]=array("type"=>"file","title"=>"Gesundheitsdaten","sample_file"=>"http://www.google.de","name"=>"gesundheitsdaten","required"=>true);

$config["hform"][]=array("type"=>"text","title"=>"Name der Krankenversicherung","name"=>"krankenversicherung","placeholder"=>"Bitte gebe Den Namen Deiner Krankenversicherung ein","required"=>true,"value"=>"Musterkrankenvers");
$config["hform"][]=array("type"=>"text","title"=>"Notfall Telefonnummer","name"=>"emergency_phone","placeholder"=>"Bitte gebe Notfalltelefonnummer ein","required"=>true,"value"=>"12356789");
$config["hform"][]=array("type"=>"text","title"=>"Krankheiten / Allergien","name"=>"allergies","placeholder"=>"Bitte gebe Allergien / KRankheiten ein","required"=>true,"value"=>"Windowsallergie");
$config["hform"][]=array("type"=>"text","title"=>"Diät","name"=>"diet","placeholder"=>"Bitte gebe Diäten ein","required"=>true,"value"=>"Max 5Packungen Knusperflocken am Tag");
$config["hform"][]=array("type"=>"text","title"=>"Regelmäßioge Medikamente, TAbleten ","name"=>"medicine","placeholder"=>"Bitte gebe Medikamente ein","required"=>true,"value"=>"");
$config["hform"][]=array("type"=>"text","title"=>"Äzrtliche behandlung benötigt?","name"=>"doctor_required","placeholder"=>"Bitte gebe doctor_required ein","required"=>true,"value"=>"Muss immer nen azrt dabei seni");
$config["hform"][]=array("type"=>"text","title"=>"Welche Infomrationen sind noch wichtig?","name"=>"important_information","placeholder"=>"Bitte gebe important_information ein","required"=>true,"value"=>"Das ist seeehr wichitg");

$config["cform2"][]=array("type"=>"checkbox","title"=>"Ich bestätige die <a href=http://www.google.de>AGB</a>","name"=>"agb","required"=>true);
$config["cform2"][]=array("type"=>"checkbox","title"=>"Ich bestätige die <a href=http://www.google.de>Einverständniserklärung</a>","required"=>true);

$config["email"]="giovanni.zeitz@jugendfilmcamp.de";

// Kofiguration Workshops
$workshops["regie"]=array("title"=>"Regie","description"=>"Regie halt");
$workshops["kamera"]=array("title"=>"Kamera/Ton/Schnitt","description"=>"Kamera und so halt");
$workshops["schauspiel"]=array("title"=>"Schauspiel","description"=>"???");
$workshops["dokumentarfilm"]=array("title"=>"Dokumentarfilm","description"=>"Grundlagen Dokumentarfilm & politischer Film, Dokumentarfilmdramaturgie, Geschichten in der Wirklichkeit, Entwurf, Entwicklung, Dreh & Postproduktion eines Dokumentarkurzfilms in Teamarbeit.");
$workshops["drehbuch"]=array("title"=>"Drehbuch","description"=>"Grundlagen im Drehbuchschreiben, Filmdramaturgie, Stoffentwicklung, Dialoge, Figurenaufbau, szenisches Schreiben, Entwurf & Entwicklung eines eigenen Drehbuches.");
$workshops["drohnenfliegen"]=array("title"=>"Drohnenfliegen","description"=>"Grundlagen & Vertiefung im Drohnenfliegen, Luftverkehrsordnung, inkl. Kenntnisnachweis für Modellflugsportler, Theorie & Praxis im Drohnenfliegen, Abschlussprüfung für camp-internen Flugschein mit Zertifikat.");
$workshops["vr"]=array("title"=>"Virtual Reality Game Design","description"=>"VR halt");
$workshops["photography"]=array("title"=>"Digital Photography","description"=>"Photo halt");
$workshops["maske"]=array("title"=>"Maskenbild","description"=>"Schminke halt");
$workshops["sound"]=array("title"=>"Sound Design","description"=>"Sound Design!!!");
$workshops["musik"]=array("title"=>"Filmmusik","description"=>"Musik halt");
$workshops["licht"]=array("title"=>"Lichtgestaltung","description"=>"Licht halt");
$workshops["szeno"]=array("title"=>"Szenografie","description"=>"Szenokram");
$workshops["stunt"]=array("title"=>"Stunt und Action","description"=>"Stunt halt");



// Konfiguration Camps

// Starter
$config["camps"]["starter"]["title"]="Starter";
$config["camps"]["starter"]["description"]="Für Einsteiger von 12-15 Jahren - im IDA-Intergrationsdorf Arendsee - erste Erfahrungen sammeln - kreative Sommerferien - neue Freunde finden - einen kompletten Film drehen.";
$config["camps"]["starter"]["image"]="http://www.jugendfilmcamp.de/images/player/starter.jpg";
$config["camps"]["starter"]["age"]=array("start"=>"12","end"=>"15");
$config["camps"]["starter"]["charge"]=380;
$config["camps"]["starter"]["charge_reduced"]=350;

// Classic
$config["camps"]["classic"]["title"]="Classic";
$config["camps"]["classic"]["description"]="Der Klassiker für alle ab 16 Jahren - auf dem Campareal - Filmemachen in der Praxis lernen - in einer Woche von der Idee zum fertigen Film - zahlreiche Workshops zum Vertiefen.";
$config["camps"]["classic"]["image"]="http://www.jugendfilmcamp.de/images/player/classic.jpg";
$config["camps"]["classic"]["age"]=array("start"=>"16","end"=>"99");
$config["camps"]["classic"]["charge"]=420;
$config["camps"]["classic"]["charge_reduced"]=400;


// Camp Zeiträume
$config["camps"]["classic"]["camps"][1]["start"]="2018-06-25";
$config["camps"]["classic"]["camps"][1]["end"]="2018-07-01";
$config["camps"]["classic"]["camps"][1]["status"]="orange";
$config["camps"]["classic"]["camps"][2]["start"]="2018-07-02";
$config["camps"]["classic"]["camps"][2]["end"]="2018-07-08";
$config["camps"]["classic"]["camps"][2]["status"]="green";
$config["camps"]["classic"]["camps"][3]["start"]="2018-07-09";
$config["camps"]["classic"]["camps"][3]["end"]="2018-07-15";
$config["camps"]["classic"]["camps"][3]["status"]="green";
$config["camps"]["classic"]["camps"][4]["start"]="2018-07-16";
$config["camps"]["classic"]["camps"][4]["end"]="2018-07-22";
$config["camps"]["classic"]["camps"][4]["status"]="orange";
$config["camps"]["classic"]["camps"][5]["start"]="2018-06-25";
$config["camps"]["classic"]["camps"][5]["end"]="2018-07-01";
$config["camps"]["classic"]["camps"][5]["status"]="orange";
$config["camps"]["classic"]["camps"][6]["start"]="2018-07-02";
$config["camps"]["classic"]["camps"][6]["end"]="2018-07-08";
$config["camps"]["classic"]["camps"][6]["status"]="green";
$config["camps"]["classic"]["camps"][7]["start"]="2018-07-07";
$config["camps"]["classic"]["camps"][7]["end"]="2018-07-15";
$config["camps"]["classic"]["camps"][7]["status"]="orange";
$config["camps"]["classic"]["camps"][8]["start"]="2018-07-16";
$config["camps"]["classic"]["camps"][8]["end"]="2018-07-22";
$config["camps"]["classic"]["camps"][8]["status"]="orange";
$config["camps"]["classic"]["camps"][9]["start"]="2018-06-25";
$config["camps"]["classic"]["camps"][9]["end"]="2018-07-01";
$config["camps"]["classic"]["camps"][9]["status"]="orange";
$config["camps"]["classic"]["camps"][10]["start"]="2018-07-02";
$config["camps"]["classic"]["camps"][10]["end"]="2018-07-08";
$config["camps"]["classic"]["camps"][10]["status"]="green";
$config["camps"]["classic"]["camps"][11]["start"]="2018-07-09";
$config["camps"]["classic"]["camps"][11]["end"]="2018-07-15";
$config["camps"]["classic"]["camps"][11]["status"]="green";

$config["camps"]["starter"]["camps"][1]["start"]="2018-07-16";
$config["camps"]["starter"]["camps"][1]["end"]="2018-07-22";
$config["camps"]["starter"]["camps"][1]["status"]="green";
$config["camps"]["starter"]["camps"][2]["start"]="2018-07-16";
$config["camps"]["starter"]["camps"][2]["end"]="2018-07-22";
$config["camps"]["starter"]["camps"][2]["status"]="green";
$config["camps"]["starter"]["camps"][3]["start"]="2018-07-16";
$config["camps"]["starter"]["camps"][3]["end"]="2018-07-22";
$config["camps"]["starter"]["camps"][3]["status"]="green";
$config["camps"]["starter"]["camps"][4]["start"]="2018-07-16";
$config["camps"]["starter"]["camps"][4]["end"]="2018-07-22";
$config["camps"]["starter"]["camps"][4]["status"]="green";
$config["camps"]["starter"]["camps"][5]["start"]="2018-07-16";
$config["camps"]["starter"]["camps"][5]["end"]="2018-07-22";
$config["camps"]["starter"]["camps"][5]["status"]="green";
$config["camps"]["starter"]["camps"][6]["start"]="2018-07-16";
$config["camps"]["starter"]["camps"][6]["end"]="2018-07-22";
$config["camps"]["starter"]["camps"][6]["status"]="green";
$config["camps"]["starter"]["camps"][7]["start"]="2018-07-16";
$config["camps"]["starter"]["camps"][7]["end"]="2018-07-22";
$config["camps"]["starter"]["camps"][7]["status"]="green";
$config["camps"]["starter"]["camps"][8]["start"]="2018-07-16";
$config["camps"]["starter"]["camps"][8]["end"]="2018-07-22";
$config["camps"]["starter"]["camps"][8]["status"]="green";
// Camp Workshops
$config["camps"]["classic"]["camps"][1]["workshops"]["regie"]["status"]="red";
$config["camps"]["classic"]["camps"][1]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["classic"]["camps"][1]["workshops"]["schauspiel"]=$workshops["schauspiel"];
$config["camps"]["classic"]["camps"][1]["workshops"]["drehbuch"]=$workshops["drehbuch"];
$config["camps"]["classic"]["camps"][1]["workshops"]["drohnenfliegen"]["status"]="green";

$config["camps"]["classic"]["camps"][2]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["classic"]["camps"][2]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["classic"]["camps"][2]["workshops"]["schauspiel"]=$workshops["schauspiel"];
$config["camps"]["classic"]["camps"][2]["workshops"]["vr"]=$workshops["vr"];
$config["camps"]["classic"]["camps"][2]["workshops"]["photography"]=$workshops["photography"];

$config["camps"]["classic"]["camps"][3]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["classic"]["camps"][3]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["classic"]["camps"][3]["workshops"]["schauspiel"]=$workshops["schauspiel"];
$config["camps"]["classic"]["camps"][3]["workshops"]["maske"]=$workshops["maske"];
$config["camps"]["classic"]["camps"][3]["workshops"]["sound"]=$workshops["sound"];

$config["camps"]["classic"]["camps"][4]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["classic"]["camps"][4]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["classic"]["camps"][4]["workshops"]["schauspiel"]=$workshops["schauspiel"];
$config["camps"]["classic"]["camps"][4]["workshops"]["drehbuch"]["status"]="red";
$config["camps"]["classic"]["camps"][4]["workshops"]["stunt"]=$workshops["stunt"];

$config["camps"]["classic"]["camps"][5]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["classic"]["camps"][5]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["classic"]["camps"][5]["workshops"]["schauspiel"]=$workshops["schauspiel"];
$config["camps"]["classic"]["camps"][5]["workshops"]["musik"]=$workshops["musik"];
$config["camps"]["classic"]["camps"][5]["workshops"]["stunt"]=$workshops["stunt"];


// -----------------------------------STARTER---------------------------------------------------------

$config["camps"]["starter"]["camps"][1]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["starter"]["camps"][1]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["starter"]["camps"][1]["workshops"]["schauspiel"]=$workshops["schauspiel"];

$config["camps"]["starter"]["camps"][2]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["starter"]["camps"][2]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["starter"]["camps"][2]["workshops"]["schauspiel"]=$workshops["schauspiel"];

$config["camps"]["starter"]["camps"][3]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["starter"]["camps"][3]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["starter"]["camps"][3]["workshops"]["schauspiel"]=$workshops["schauspiel"];

$config["camps"]["starter"]["camps"][4]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["starter"]["camps"][4]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["starter"]["camps"][4]["workshops"]["schauspiel"]=$workshops["schauspiel"];

$config["camps"]["starter"]["camps"][5]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["starter"]["camps"][5]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["starter"]["camps"][5]["workshops"]["schauspiel"]=$workshops["schauspiel"];

$config["camps"]["starter"]["camps"][6]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["starter"]["camps"][6]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["starter"]["camps"][6]["workshops"]["schauspiel"]=$workshops["schauspiel"];

$config["camps"]["starter"]["camps"][7]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["starter"]["camps"][7]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["starter"]["camps"][7]["workshops"]["schauspiel"]=$workshops["schauspiel"];

$config["camps"]["starter"]["camps"][8]["workshops"]["regie"]=$workshops["regie"];
$config["camps"]["starter"]["camps"][8]["workshops"]["kamera"]=$workshops["kamera"];
$config["camps"]["starter"]["camps"][8]["workshops"]["schauspiel"]=$workshops["schauspiel"];

