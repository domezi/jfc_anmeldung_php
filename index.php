<?php
error_reporting(0);
session_start();
$VERSION="1.4";

/*
Hi Giovanni,
nicht wundern, die Konfiguration habe ich mal in die config.php ausgelagert.
War mir zu viel in dieser Datei...
*/
require("config.php");



// Ab hier nichts mehr ändern

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://jugendfilmcamp.de/css/itsthenewstyle.css">

    <meta name="msapplication-TileColor" content="#f0ab3f" />
    <meta name="theme-color" content="#f0ab3f">
    <meta name="msapplication-navbutton-color" content="#f0ab3f">
    <meta name="apple-mobile-web-app-status-bar-style" content="#f0ab3f">

    <script>
    $( function() {
        $( "#datepicker" ).datepicker({ "dateFormat": "dd.mm.yy" });
    } );
    </script>
    <style>
    .form-control {
        margin-bottom:10px;margin-top:-10px;
        width: 100%;
        padding: 22px;
        border: none;
        -moz-box-shadow: inset 3px 4px 5px rgba(0,0,0,0.15);
        -webkit-box-shadow: inset 3px 4px 5px rgba(0,0,0,0.15);
        box-shadow: inset 3px 4px 5px rgba(0,0,0,0.15);
        background:#ffffff22;
        color:lightgray;
    } .custom-file {
        margin:20px 0;
        width: 100%;
        border: none;
        -moz-box-shadow: inset 3px 4px 5px rgba(0,0,0,0.15);
        -webkit-box-shadow: inset 3px 4px 5px rgba(0,0,0,0.15);
        box-shadow: inset 3px 4px 5px rgba(0,0,0,0.15);
    } .custom-file * {
        background:#ffffff22;
        color:lightgray;
    } .custom-select {
        margin-bottom:10px;
        margin-top:0px;
        padding: 3px 22px;
        border: none;
        -moz-box-shadow: inset 3px 4px 5px rgba(0,0,0,0.15);
        -webkit-box-shadow: inset 3px 4px 5px rgba(0,0,0,0.15);
        box-shadow: inset 3px 4px 5px rgba(0,0,0,0.15);
        background:#ffffff22;
        color:gray;
    }
    nav {
        float:none;
    }
    body {
        background-color: #27333D;
        background-image:url(http://jugendfilmcamp.de/images/banner/paralax-verwirrt.jpg);
        background-size:cover;
        color:white;
        min-height:100vh;
    }
    .alert-danger {
        position:inherit;
        margin-bottom: 30px;
        padding: 15px 30px 15px 30px;
        font-size: 0.9em;
        border:none;
        background: #F0AB3F;
    }
    .bg-orange {
        background:#F0AB3F
    }
    .color-orange, h1, h2 {
        color:#F0AB3F;
    }
    .card:hover {
        box-shadow: 0 0 10px #f0ab3f;
    }
    .card-title {
        font-weight: 400;
        letter-spacing: 0.025em;
        line-height: 1.5;
        text-transform: uppercase;
        font-size:1.6em;
        text-align:center;
    }
    .card .btn {
        width:100%;
    }
    nav ol.breadcrumb {
        background-color: #27333D;
        color:white;
        border:none;
    }
    a {
        color: #f0ab3f;
    }
    a:hover {
        color: orange;
    }
    .card {
        background-color: #27333D;
        color:white;
        border:none;
    }
    .card.card-starter {
        background-color: #f0ab3f;
        border:none;
    }
    .card.card-classic {
        background-color: #27333D;
        color:white;
        border:none;
    }
    .card.card-starter .btn.btn-primary {
        border: 2px solid #f0ab3f;
        background-color: #27333D;
        color:#f0ab3f;
    }
    .card.card-starter .btn.btn-primary:hover {
        color:#27333D;
        background-color:#f0ab3f;
    }
    .btn {
        font-weight: 600;
        text-transform: uppercase;
        box-shadow: 1px 3px 15px rgba(0, 0, 0, 0.4);
        transition: all 0.2s;
        padding:10px 20px;
    }
    .btn.btn-primary  {
        color:#27333D;
        border:0;
        background-color: #f0ab3f;
    }
    .btn.btn-secondary  {
        color:#f0ab3f;
        border: 2px solid #f0ab3f;
        background-color: #27333D;
    }
    .btn.btn-primary:hover {
        color: #e38b00;
        background: #27333d;
    }
    .btn.btn-secondary:hover {
        color: #27333d;
        background: #e38b00;
    }
    h1 {
        margin-bottom:0px;
    }
    @media (max-width:992px) {
        .card {
            margin-bottom:17px;
            width:100% !important;
        }
        h1 {
            font-size:30px;
            text-align:center;
        }
        h2 {
            font-size:20px;
            text-align:center;
            margin-bottom:0px;

        }
        .btn {
            width:100%;
        }
    }
    body {
        padding-bottom:20px;
    }
    </style>

    <title>JFC Camp Anmeldung</title>
  </head>
  <body>

    <div class=container style="margin-top:25px">
    <h2>Anmeldung <?php echo $_SESSION["camp"]; ?></h2>
    <img src=http://jugendfilmcamp.de/images/layout/underline.svg class="mb-4 d-md-none">
    <?php

    if($_GET["step"]>1 && count($_SESSION)<1) echo "<meta http-equiv=refresh content=0,?step=0>";

    function isAgeWithin($camp,$bday=null,$use_end=false) {
      $bday_time=strtotime($bday);
      $age_now_seconds=getAgeWithin($camp,$bday,$use_end);
      $age_min_seconds =$camp["age"]["start"]*3600*24*365;
      $age_max_seconds=$camp["age"]["end"]*3600*24*365;
      return ($age_now_seconds > $age_min_seconds && $age_now_seconds < $age_max_seconds);
    }
    function getAgeWithin($camp,$bday=null,$use_end=false) {
      if($bday==null) {
        $bday=$_POST["birthdate"];
      }
      $bday_time=strtotime($bday);

      if($use_end)
          $time_of_decision = strtotime($camp["end"]);
      else
          $time_of_decision = strtotime($camp["start"]);

      $age_now_seconds=$time_of_decision-$bday_time; //replace time() // toleranz, muss innerhalb der woche das alter erreichen
      $age_min_seconds =$camp["age"]["start"]*3600*24*365;
      $age_max_seconds=$camp["age"]["end"]*3600*24*365;
      return $age_now_seconds; //31536000
    }

    // schritt bestimmen
    $step=intval($_GET["step"]);

    // handle responses
    if($_GET["action"]=="startover") {
        session_destroy();
        session_start();
    } else if(isset($_GET["camp"])) {
        $_SESSION["camp"]=$_GET["camp"];
    } else if(isset($_POST["step1_submit"])) {

        unset($_POST["step1_submit"]);

        foreach($_FILES as $slug=>$file) {
            $path_parts = pathinfo($file["name"]);
            $filename=strtolower(str_replace(" ","_",$_POST["nachname"]." ".$_POST["vorname"]))."_".$slug."_".time().".".$path_parts['extension'];
            if(!move_uploaded_file($file["tmp_name"],"uploads/".$filename)) {
                $msg2="Upload Fehler.";
            }
            $_POST[$slug]=$filename;
        }


        if($_POST["email"]!=$_POST["email_confirm"])
            $msg2="Die angegebenen Email-Adressen stimmen nicht überein.";


        unset($_POST["email_confirm"]);
       
        $_SESSION["cform"]=$_POST;


        /*
        //date in mm/dd/yyyy format; or it can be in other formats as well
        $birthDate = date("m/d/Y",strtotime($_POST["birthdate"]));
        //explode the date to get month, day and year
        $birthDate = explode("/", $birthDate);
        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
        ? ((date("Y") - $birthDate[2]) - 1)
        : (date("Y") - $birthDate[2]));
        */

        // check dates
        /*
        if($age<$config["camps"][$_SESSION["camp"]]["age"]["start"]) {
            $msg="jung";
        } else if($age>$config["camps"][$_SESSION["camp"]]["age"]["end"]) {
            $msg="alt";
        }
        */

        if(!isAgeWithin($config["camps"][$_SESSION["camp"]],$_POST["birthdate"],true)) {
          $msg="jung/alt";
        }


        // display error
        if($msg) {
            $step--;
            $alert= "<div class='alert alert-danger'>Ohje, das tut uns Leid! Du bist zu ".$msg." zur Teilnahme am Camp ".$config["camps"][$_SESSION["camp"]]["title"]."
              (Alterbereich: ".$config["camps"][$_SESSION["camp"]]["age"]["start"]."-".$config["camps"][$_SESSION["camp"]]["age"]["end"].", Dein Alter: ".round(getAgeWithin($config["camps"][$_SESSION["camp"]])/31536000).")</div>";
        } else if($msg2) {
            $step--;
            $alert= "<div class='alert alert-danger'>".$msg2."</div>";
        }

    } else if(isset($_POST["step2_submit"])) {

        foreach($_POST["camp"] as $index=>$true) {
            $weeks[$index]=array("primary"=>$_POST["camp".$index."_prio1"],"secondary"=>$_POST["camp".$index."_prio2"]);
            if($_POST["camp".$index."_prio1"] != "" && $_POST["camp".$index."_prio2"] == "" && $_POST["camp".$index."_no2ndPrio"] == "true")
                $workshop_ok++;
            else if($_POST["camp".$index."_prio1"] == "" || $_POST["camp".$index."_prio2"] == "")
                $workshop_empty++;
            else if($_POST["camp".$index."_prio1"]==$_POST["camp".$index."_prio2"])
                $workshop_equal++;
            else
                $workshop_ok++;
        }

        // check
        if(!count($_POST["camp"]))
            $msg="Bitte wähle mindestens eine Woche aus.";
        else if($workshop_empty)
            $msg="Bitte wähle jeweils einen Workshop aus.";
        else if($workshop_equal)
            $msg="Bitte wähle verschiedene Workshops aus.";

        // display error
        if($msg) {
            $step--;
            $alert= "<div class='alert alert-danger'>".$msg."</div>";
        } else {
            // save to session
            $_SESSION["weeks"]=$weeks;
        }

    }

    // breadcrumb anzeigen
    echo '<nav aria-label="breadcrumb" class="d-none d-md-block"><ol class="breadcrumb">';
//    echo '<li class="breadcrumb-item"><a href="?step=0&action=startover">Anmeldung</a></li>';
    if($step > 0 )
        echo '<li class="breadcrumb-item"><a href="?step=0">'.$config["camps"][$_SESSION["camp"]]["title"].'</a></li>';
    if($step > 1)
        echo '<li class="breadcrumb-item"><a href="?step=1">Persönliche Daten</a></li>';
    if($step > 2)
        echo '<li class="breadcrumb-item"><a href="?step=2">Wochen Auswahl</a></li>';
    if($step > 3)
        echo '<li class="breadcrumb-item"><a href="?step=3">Krankheiten/Allergien</a></li>';
    if($step==0)
        echo '<li class="breadcrumb-item">Camp wählen</li>';
    if($step==1)
        echo '<li class="breadcrumb-item">Persönliche Daten</li>';
    if($step==2)
        echo '<li class="breadcrumb-item">Wochen Auswahl</li>';
    if($step==3)
        echo '<li class="breadcrumb-item">Krankheiten/Allergien</li>';
    if($step==4)
        echo '<li class="breadcrumb-item">Bestätigen</li>';
    if($step==5)
        echo '<li class="breadcrumb-item">Abgeschlossen</li>';
    echo '</ol></nav>';

    if($alert) echo $alert;
    
    if($config["steps"][$_GET["step"]]) {
        echo "<div style='border-bottom:1px solid gray;padding-bottom:15px;margin-bottom:25px;'><b style=font-size:1.4em;color:#F0AB3F>".$config["steps"][$_GET["step"]]["title"]."</b>";
        echo "<br>".$config["steps"][$_GET["step"]]["description"]."";
        echo "</div>";
    }

    // formular anzeigen
    if($step==0) {
        foreach($config["camps"] as $key=>$camp) {
            echo '<div class="card card-'.$key.'" style="float:left;margin-right:10px;width: 18rem;">
              <img class="card-img-top" src="'.$camp["image"].'" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">'.$camp["title"].'</h5>
                <p class="card-text">'.$camp["description"].'</p>
                <a href="?camp='.$key.'&step=1" class="btn btn-primary">Dieses Camp wählen</a>
              </div>
            </div>';
        }
    } else if($step==1) {
        // create contact form
        echo "<form method=post action=?step=2 enctype=multipart/form-data>";
        foreach($config["cform"] as $key=>$input) {
            if($input["type"]=="checkbox") {
                echo '<div class="custom-control custom-checkbox">
                  <input  ';
                    if($input["required"]) echo "required=true ";
                    echo 'type="checkbox" name="'.$input["name"].'" value=Ja class="custom-control-input" id="customCheck'.$input["name"].'">
                  <label class="custom-control-label" for="customCheck'.$input["name"].'">'.$input["title"].'</label>
                </div>';
            } else if($input["type"]=="select") {
                echo '<div>'.$input["title"].'
                  <select';
                    if($input["required"]) echo " required=true ";
                    echo ' class="custom-select" name="'.$input["name"].'" id="'.$input["name"].'">
                    <option>'.implode("</option><option>",$input["options"]).'</option>
                    </select>
                </div>';
            } else if($input["type"]=="file") {
                echo '<div class="custom-file" >
                  <input';
                    if($input["required"]) echo " required=true ";
                    echo ' type="file" class="custom-file-input" name="'.$input["name"].'" id="customFile'.$input["name"].'">
                  <label class="custom-file-label" for="customFile'.$input["name"].'">Datei "'.$input["title"].'" auswählen</label>
                    <a style=background:none target=_blank href='.$input["sample_file"].'>&rarr; Vorlage für "'.$input["title"].'" herunterladen</a>
                </div>';
            } else {
                echo "<label>".$input["title"]."";
                if($input["required"]) echo "*";
                echo "</label><input type='".$input["type"]."' name='".$input["name"]."'  value='".env($_POST[$input["name"]],env($_SESSION["cform"][$input["name"]],$input["value"]))."' ";
                if($input["required"]) echo "required=true";
                echo " class='form-control'";
                echo " placeholder='".$input["placeholder"]."' id='".$input["id"]."'>";
            }
        }
        echo "<input class='btn btn-primary' style=margin-top:20px name=step1_submit value='Weiter' type=submit>";
        echo "</form>";
        //echo "<a class='d-md-none mt-2 btn btn-secondary' href=?step=0>Zurück</a>";
    } else if($step==2) {
        echo "<form method=post action=?step=3>";
        echo "Bitte wähle ein oder mehrere Wochen aus:";
        
        //mlx25if($_SESSION["weeks"]
        foreach($_SESSION["weeks"] as $key=>$week) {
            $_POST["camp"][$key]=1;
            $_POST["camp".$key.'_prio1']=$week["primary"];
            $_POST["camp".$key.'_prio2']=$week["secondary"];
        }


        foreach($config["camps"][$_SESSION["camp"]]["camps"] as $key=>$camp) {
            $disabled=(!isAgeWithin($camp,$_SESSION["cform"]["birthdate"]));
            if($disabled) continue;
            echo '<div class=card style=padding:10px;margin-top:10px><div class="custom-control custom-checkbox">
                <input onchange=\'$(".camp'.$key.'_workshops").toggle();\' ';if($_POST["camp"][$key]=="1")echo " checked ";echo'';if($camp["status"]=="red")echo " disabled ";echo' type="checkbox" class="custom-control-input" name=camp['.$key.'] value=1 id="customCheck'.$key.'">
                <label class="custom-control-label" for="customCheck'.$key.'">Camp '.$key.' ('.date("d.m",strtotime($camp["start"])).' - '.date("d.m.Y",strtotime($camp["end"])).') '.get_badge($camp["status"]).'</label>
            </div>
            <div class=camp'.$key.'_workshops style=display:';if($_POST["camp"][$key]!="1")echo "none";echo';>';
            for($i=1;$i<=2;$i++) {
                echo "<div style=padding-left:20px><hr>".$i.". Wahl des Workhops:";
                foreach($camp["workshops"] as $wkey=>$workshop) {
                    echo '<div class="custom-control custom-radio">
                      <input type="radio" id="camp'.$key.'_prio'.$i.'_workshop'.$wkey.'" ';if($_POST["camp".$key.'_prio'.$i]==$wkey)echo " checked ";echo'';if($workshop["status"]=="red")echo " disabled ";echo' name="camp'.$key.'_prio'.$i.'" value='.$wkey.' class="custom-control-input">
                      <label class="custom-control-label" for="camp'.$key.'_prio'.$i.'_workshop'.$wkey.'">'.$workshop["title"].' '.get_badge($workshop["status"]).'
</label>
                    </div>';
                    if($workshop["status"]=="red")
                        $workshops_red++;
                }
                echo "</div>";
                if( ( count($camp["workshops"])-$workshops_red ) ==1) {
                    $i++;
                    echo "<input type=hidden name='camp".$key."_no2ndPrio' value=true>";
                }
            }
            if( ( count($camp["workshops"])-$workshops_red ) > 1) {
                echo "<hr>Deine 1. und 2. Wahl müssen sich unterscheiden!";
            } else echo "<br>";
            echo '</div>
            </div>';
        }
        echo "<a class='mt-0 btn btn-secondary' style=border:none onclick=\"window.history.back();\">Zurück</a>&nbsp;";
        echo "<input style='margin:10px 0' class='btn btn-primary' name=step2_submit value='Weiter' type=submit>";
        echo "</form>";
    } else if($step==3) {

        // create health form
        echo "<form method=post action=?step=4 enctype=multipart/form-data>";
        foreach($config["hform"] as $key=>$input) {

            echo "<label>".$input["title"]."";
            if($input["required"]) echo "*";
            echo "</label><input type='".$input["type"]."' name='".$input["name"]."'  value='".env($_POST[$input["name"]],env($_SESSION["hform"][$input["name"]],$input["value"]))."' ";
            if($input["required"]) echo "required=true";
            echo " class='form-control'";
            echo " placeholder='".$input["placeholder"]."' id='".$input["id"]."'>";

            echo '<div class="custom-control custom-checkbox" style=margin-top:-5px>
              <input onchange=\'if( $(this).is(":checked") ) { $(this).parent().prev("input").removeAttr("required"); } else { $(this).parent().prev("input").attr("required","required"); }\' type="checkbox" name="'.$input["name"].'" value="" class="custom-control-input" id="customCheck'.$input["name"].'">
              <label class="custom-control-label"  for="customCheck'.$input["name"].'">Ich möchte das Feld "'.$input["title"].'" leerlassen</label>
            </div><br>';

        }
        echo "<a class='mt-0 btn btn-secondary' style=border:none onclick=\"window.history.back();\">Zurück</a>&nbsp;";
        echo "<input class='btn btn-primary' name=submit3  name=step3_submit value='Weiter' type=submit>";
        echo "</form>";

    } else if($step==4) {

        


        if(isset($_POST["submit3"])) {
            foreach($_POST as $key=>$val) {
                if($_POST[$key]=="")
                    $_POST[$key]="LEER";
            }
            $_SESSION["hform"]=$_POST;
        }
        unset($_SESSION["hform"]["submit3"]);


        ob_start();
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $actual_link = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];

        echo "<h2>Persönliches:</h2><table class='table table-striped'>";
        foreach($config["cform"] as $key=>$input) {
            $val=$_SESSION["cform"][$input["name"]];
            if($config["cform"][$key]["type"]=="file") $val="<a target=_blank href=".$actual_link."/uploads/".$_SESSION["cform"][$input["name"]].">Download ".$input["title"]."</a>";
            echo "<tr><td>".$input["title"]."</td><td>".$val."</td></tr>";
        }
        echo "</table>";

        echo "<h2>Gesundheitliches:</h2><table class='table table-striped'>";
        foreach($config["hform"] as $key=>$input) {
            $val=$_SESSION["hform"][$input["name"]];
            echo "<tr><td>".$input["title"]."</td><td>".$val."</td></tr>";
        }
        echo "</table>";

        $reduced=($_SESSION["cform"]["known"]);

        if($reduced) $_SESSION["cform"]["known"]="Ja"; else $_SESSION["cform"]["known"]="Nein";

        echo "<h2>Gewählte Woche(n):</h2><table class='table table-striped'>";
        foreach($_SESSION["weeks"] as $key=>$week) {

            if($reduced)
                $charge="<span style=color:orange>".$config["camps"][$_SESSION["camp"]]["charge_reduced"].".00&euro;<br>(ermäßigt)</span>";
            else
                $charge=$config["camps"][$_SESSION["camp"]]["charge"].".00&euro;";


            if($config["camps"][$_SESSION["camp"]]["camps"][$key]["workshops"][$week["secondary"]]["title"]=="")
                $config["camps"][$_SESSION["camp"]]["camps"][$key]["workshops"][$week["secondary"]]["title"]="<i>Zweitwahl nicht möglich</i>";

            echo "<tr><td><b>Woche ".$key."</b><br><kbd>".strtoupper($config["camps"][$_SESSION["camp"]]["title"])."</kbd></td><td>
                1. Wahl: ".$config["camps"][$_SESSION["camp"]]["camps"][$key]["workshops"][$week["primary"]]["title"]."<br>
                <span style=opacity:.7>2. Wahl: ".$config["camps"][$_SESSION["camp"]]["camps"][$key]["workshops"][$week["secondary"]]["title"]."</span><br>
            </td><td style=text-align:right>+ ".$charge."</td></tr>";

            $_SESSION["weeks"][$key]["charge"]=$charge;

            $sum+=intval(strip_tags($charge));

            // sorge für ermässigungen für nachfolgende wochen
            $reduced=true;
        }

        echo "<tr><td></td><td></td><td style=text-align:right><b>= <u>".$sum.".00&euro;</u></b></td></tr>";
        echo "</table>";

        $out2 = ob_get_contents();
        ob_end_clean();

$_SESSION["out2"]=$out2;

        echo $out2;

        // show cform 2
        echo "<h2>Rechtliches:</h2>";
        echo "<form action=?step=5 method=post style=margin-top:5px;>";
        foreach($config["cform2"] as $key=>$input) {
            if($input["type"]=="checkbox") {
                echo '<div class="custom-control custom-checkbox">
                  <input  ';
                    if($input["required"]) echo "required=true ";
                    echo 'type="checkbox" name="'.$input["name"].'" value=Ja class="custom-control-input" id="customCheck'.$input["name"].'">
                  <label class="custom-control-label" for="customCheck'.$input["name"].'">'.$input["title"].'</label>
                </div>';
            }
        }
        echo "<div style='border-bottom:1px solid white;margin:20px 0'></div>";

        echo "<br><textarea width=100%  style='max-width:100% !important' class=form-control name=submit_comment placeholder='Kommentare?'></textarea><input type=submit name=submit4 style=width:100%;margin-top:5px;padding:20px  class='btn btn-primary' value='Verbindlich Anmelden'></form>";
        echo "<a class='mt-3 btn btn-secondary' style=border:none onclick=\"window.history.back();\">Zurück</a>&nbsp;";
    } else if($step==5) {

        $uniqid=strtoupper(uniqid());

        $_SESSION["submit_comment"]=$_POST["submit_comment"];

        $out2=$_SESSION["out2"];
        unset($_SESSION["out2"]);
        sendmail($_SESSION["cform"]["email"],"Vielen Dank für Deine Anmeldung beim Jugendfilmcamp, ".$_SESSION["cform"]["vornameteil"],"<h1>Vielen Dank ".$_SESSION["cform"]["vornameteil"].", hiermit bestätigen wird Deine Anmeldung (Buchung-Nr: $uniqid).</h1>".$out2);
        if(strlen($_SESSION["cform"]["email_cc"])) sendmail($_SESSION["cform"]["email_cc"],"CC: Vielen Dank für Deine Anmeldung beim Jugendfilmcamp, ".$_SESSION["cform"]["vornameteil"],"<h1>Vielen Dank ".$_SESSION["cform"]["vornameteil"].", hiermit bestätigen wird Deine Anmeldung (Buchung-Nr: $uniqid).</h1>".$out2);
        sendmail($config["email"],"Neue Anmeldung \"".$_SESSION["cform"]["vornameteil"]." ".$_SESSION["cform"]["nachnameteil"]."\" beim Jugendfilmcamp","<h1>Es gibt eine neue Anmeldung (Buchung-Nr: $uniqid):</h1>".$out2);

        $out=json_decode(file_get_contents("anmeldungen.json"),1);
        $_SESSION["modstamp"]=date("Y-m-d H:i:s");
        $out[$uniqid]=$_SESSION;
        if(file_put_contents("anmeldungen.json",json_encode($out))) {
            echo "<div class=text-center style=margin-top:70px><h1 style=color:White>Vielen Dank!</h1><p>Sie erhalten in Kürze eine Bestätigungs-Email.</p>
            <a class='btn btn-secondary mt-4' style='width:auto !important' href=http://jugendfilmcamp.de/>Zurück zur Webseite</a></div>";
            session_destroy();
        } else
            echo "<div class=text-center style=margin-top:70px><h1>Entschuldigung!</h1><p>Es ist ein Fehler aufgetreten.</p></div>";
    }


    function env($v,$d) {
        return($v)?$v:$d;
    }

    function get_badge($status) {
        if($status=="green")
            return '<span class="badge badge-success">Plätze verfügbar</span>';
        if($status=="orange")
            return '<span class="badge badge-warning" style=color:white>Wenig Plätze</span>';
        if($status=="red")
            return '<span class="badge badge-danger">Ausgebucht</span>';
    }


    function sendmail($to,$subject,$body) {

        global $config;

        $headers = "From: Jugendfilmcamp Arendsee <".$config["email"].">\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";

        $body=''.str_replace(array("ä","ü","ö","Ä","Ö","Ü","ß"),array("&auml;","&uuml;","&ouml;","&Auml;","&Uuml;","&Ouml;","&szlig;"),$body).'<br><br>';

        $output=file_get_contents("mail.php");

        mail($to,$subject,"<html>".str_replace("###BODY###",$body,$output),$headers);
    }
    ?>
    </div>
  </body>
</html>
