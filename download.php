<?php
$PASS_SHA512=hash("sha512","M4J4-2018!");
// $PASS_SHA512="defa48e6df2af6adedfaa48e6df2af6adedfa90fda7654da90fda7654daa48e6df2af6adedfa90fda7654daa48e6df2af6adedfa90fda7654daa48e6df2af6adedfa90fda7654da"; // password hashed with sha512


if(hash("sha512",$_POST["pass"])!=$PASS_SHA512) die('<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
    $( ".datepicker" ).datepicker({ "dateFormat":"dd.mm.yy"});
    } );
    </script>
    <link rel="stylesheet" href="http://jugendfilmcamp.de/css/itsthenewstyle.css"><style>body {
        background-color: #27333D;
        background-image:url(http://jugendfilmcamp.de/images/banner/paralax-verwirrt.jpg);
        background-size:cover;
        color:white;
    } 
    .btn.btn-primary  {
        color:#27333D;
        border:0;
        background-color: #f0ab3f;
    }
    .btn.btn-primary:hover {
        color: #e38b00;
        background: #27333d;
    } .form-control,.custom-select {margin-bottom:10px}</style><div class=container style=margin-top:40px>
    '."<h3>Download Anmeldungen</h3><form method=post>
    Bitte Passwort eingeben:<input value=M4J4-2018! class=form-control placeholder=Passwort name=pass autofocus type=password>
    Auswahl Camps:<select name=camp class=custom-select><option value=all>All camps</option><option value=starter>Starter</option><option value=classic>Classic</option></select>
    <div class=row style=padding:0><div class=col-md-6 style=padding:0;padding-right:10px>Von:<input class='datepicker form-control' value='".date("d.m.Y",strtotime("-1week"))."'  autocomplete=off type=datde placeholder=Von name=start></div><div class=col-md-6 style=padding:0;>Bis:
    <input class='datepicker form-control' autocomplete=off type=datde style=text-align:rfight value='".date("d.m.Y")."' placeholder=Bis name=end></div></div>
    <input type=submit class='btn mt-2  btn-primary' style='width:100%;padding:8px;font-size:1.2em;text-transform:uppercase' value='Anmeldungen downloaden'></form>");

header("Content-Encoding: UTF-8");
header("Content-type: text/csv; charset=UTF-8");
header("Content-Disposition: attachment; filename=jfc_anmeldungen_stand_".date("Y-m-d_H-i").".csv");
header("Pragma: no-cache");
header("Expires: 0");
echo "\xEF\xBB\xBF";

$out=json_decode(file_get_contents("anmeldungen.json"),1);

// get all weeks and camps
foreach($out as $anmeldung_id=>$anmeldung) {
    foreach($anmeldung["weeks"] as $week=>$foo) {
        $camps[$anmeldung["camp"]]["weeks"][$week]=true;
    }
}

foreach($camps as $campname=>$camp) {
    
    // sollen beide csv dateien zusammengefasst werden
    if($_POST["camp"]!="all" && $campname!=$_POST["camp"]) continue;

    $scamp=strtoupper($campname);
    foreach($camp["weeks"] as $windex=>$foo) {
        foreach($out as $key=>$anmeldung) {

            // ist aktuelles camp?
            if($anmeldung["camp"]!=$campname) continue;

            foreach($anmeldung["weeks"] as $anmeldung_windex=>$anmeldung_week) {

                // gehört zu aktueller woche?
                if($anmeldung_windex!=$windex)
                    continue;

                // ist im intervall das giovanni ausgewählt hat?
                $ts=strtotime($anmeldung["modstamp"]);
                $start=strtotime($_POST["start"]);
                $end=strtotime($_POST["end"])+86400;
                if($ts < $start || $ts > ($end+1))
                    continue;

                // show user line
                echo $key.";".$scamp.";".$windex.";".$anmeldung_week["primary"].";".$anmeldung_week["secondary"].";".$anmeldung["modstamp"].";";

                // show user fields
                foreach($anmeldung["cform"] as $val) {
                    echo "\"".str_replace("\"","",$val)."\";";
                }
                // show health fields
                foreach($anmeldung["hform"] as $val) {
                    echo "\"".str_replace("\"","",$val)."\";";
                }

                echo ";".$anmeldung["submit_comment"].";".intval($anmeldung_week["charge"])."€;";

    
                $wochen_count=count($anmeldung["weeks"]);
                echo ($wochen_count==1)?"1 Woche":$wochen_count." Wochen";

                echo "\n";

            }
        }
    }
}

/*
foreach($out as $key=>$anmeldung) {
    echo $anmeldung["camp"].",,";
    foreach($anmeldung["cform"] as $val) {
        echo "\"".str_replace("\"","",$val)."\",";
    }
    foreach($anmeldung["cform"] as $val) {
        echo "\"".str_replace("\"","",$val)."\",";
    }
    echo "\n";
}
*/
